---
layout: post
title: "OpenBSD on genesis"
date: 2013-05-16 04:37 -0000
category: computing
tags: rofldell openbsd
---
A few days ago, I installed OpenBSD 5.3 on genesis.

I installed OpenBSD in a QEMU VM, dd'd its image to a USB flash drive, and then booted from it. Surprisingly OpenBSD supports all the relevant hardware. X even works at the correct resolution.

Shortly thereafter, I backed up as much data as I could, but still lost roughly 18 GB of data because I couldn't fit all the data everywhere else. I then installed OpenBSD.

I have 16 packages installed from the repositories as of this writing. I use cwm instead of the default FVWM.
