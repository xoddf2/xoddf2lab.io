---
layout: post
title: "Watch Lost Signal"
date: 2023-11-02 15:47 -0700
category: journal
tags: watch wwv
---
My [Casio WV-M60 watch](https://en.wikipedia.org/wiki/Casio_Wave_Ceptor) has not received a reliable signal from [WWVB](https://www.nist.gov/pml/time-and-frequency-division/time-distribution/radio-station-wwvb) in a few months.

Most recent successful sync was at 2023-10-06T03:05-0700, and it displayed the Level 0 receiving indicator that time (3 being the strongest).  At one point, in August, I used the manual receiving feature outdoors, during the day, and it worked fine.  (I even had a shortwave radio tuned to [WWV](https://www.nist.gov/pml/time-and-frequency-division/time-distribution/radio-station-wwv)'s 15 MHz signal, and I received a good signal from it.)

I have kept it in my bedroom window the entire time per the manual, and it previously worked properly.  Did atmospheric conditions change?
