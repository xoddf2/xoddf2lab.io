---
layout: post
title: "Making Firefox Sane"
date: 2015-12-21 17:36 -0000
category: computing
tags: firefox
---
Here are some solutions to fixing UI issues in recent versions of Firefox:

* Disable smooth scrolling (Preferences → Advanced → General → uncheck Use smooth scrolling).
* Set `browser.tabs.closeWindowWithLastTab` to `false`.
* Set `browser.newtab.url` to `about:blank`.
* Set `browser.urlbar.trimURLs` to `false`.
* Set `gfx.color_management_mode` to `0` to prevent images from appearing with a purple/indigo tint.

Some issues are bad enough to need extensions to fix:

* [Disable Ctrl-Q Shortcut](https://addons.mozilla.org/en-US/firefox/addon/disable-ctrl-q-shortcut/)
* [Expire history by days](https://addons.mozilla.org/en-US/firefox/addon/expire-history-by-days/)
* [Open in Browser](https://addons.mozilla.org/en-US/firefox/addon/open-in-browser/)
