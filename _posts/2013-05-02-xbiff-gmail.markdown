---
layout: post
title: "xbiff with Gmail"
date: 2013-05-02 06:35 -0000
category: computing
tags: email xbiff gmail
---
It is possible to use [xbiff][1] with Gmail. It involves fetchmail and IMAP.

Put this in your ~/.fetchmailrc:

    poll imap.gmail.com protocol IMAP user "(insert username here)@gmail.com" there with password "(insert password here)" nofetchall keep ssl

Then have xbiff use this X resource:

    XBiff*checkCommand: fetchmail -d0 -csk

I also recommend adding this resource to prevent it from checking so frequently:

    XBiff*update: 300

Now xbiff should check your Gmail inbox. I prefer to use it in [FvwmButtons][2].

[1]: http://en.wikipedia.org/wiki/Xbiff
[2]: http://www.fvwm.org/documentation/manpages/stable/FvwmButtons.php
