---
layout: post
title: "Back to Irssi"
date: 2021-10-28 07:58 -0700
category: computing
tags: rant irc irssi circe emacs
---
For several years, I used [Irssi](https://irssi.org/) in [tmux](https://tmux.github.io/) on a [shell server](https://en.wikipedia.org/wiki/Shell_account).  A few months ago, I started using [Circe](https://github.com/emacs-circe/circe) as my IRC client (connected to a [ZNC](https://wiki.znc.in/ZNC) bouncer) after a brief stint with [ERC](https://en.wikipedia.org/wiki/ERC_(software)).  Circe had various issues:

* Basic features, such as the display of my current nick, current channel modes, and `/LIST`, were not implemented.
* The tab completion omitted the colon after the nick, even with `circe-completion-suffix` set to the default `: `.  It also occasionally gave odd tab completion suggestions based on obscure substrings.
* The currently focused channel(s) still appeared in the tracker regardless of `lui-track-behavior`'s value.
* Even with `circe-lagmon-mode`, Circe did not reconnect to my bouncer after losing the network connection.
* `circe-lagmon-mode` did not work with some servers.
* `circe-lagmon-mode` would cause Circe to spam "`*** User away: <current nick>`" when I was set as away, at least with multiple instances of Circe running.
* Circe would set my away message to my nick instead of what I typed as an argument to `/[G]AWAY`.
* The topic was not always visible _à la_ Irssi, WeeChat, or ERC's top bar.  I would rather not type `/TOPIC` constantly.
* WHOIS output would appear in random buffers.

Early this month, I switched back to Irssi (with ZNC this time) after getting tired of Circe's bugs.  With multiple clients at least, it is not as good as bare Irssi in a terminal multiplexer, but it works better than Circe did.
