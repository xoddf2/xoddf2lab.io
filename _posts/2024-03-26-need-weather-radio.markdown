---
layout: post
title: "You Need a Weather Radio"
date: 2024-03-26 03:41 -0700
category: other
tags: weather
---
Almost every home, school, and business in the United States and Canada needs a [weather radio](https://www.weather.gov/nwr/).  It is an important piece of safety equipment on par with a smoke detector.

[NOAA Weather Radio](https://www.weather.gov/nwr/) and [Weatheradio Canada](https://www.canada.ca/en/environment-climate-change/services/weatheradio.html) are government-run radio networks that broadcast weather information and emergency alerts continuously on a dedicated band of 7 frequencies (162.4&ndash;162.55 MHz).  Many weather radio receivers are equipped with alert functionality triggered by the [<abbr title="Specific Area Message Encoding">SAME</abbr> data bursts](https://en.wikipedia.org/wiki/Specific_Area_Message_Encoding) or 1050 Hz Warning Alarm Tone.

There are multiple types of weather radios, including nightstand models with SAME alert capability, and emergency models with built-in flashlights, power banks, and the ability to charge the battery from a solar panel or hand crank.

<abbr title="Specific Area Message Encoding">SAME</abbr> functionality allows the user to exclude alerts for other counties and/or of certain types.

A weather radio provides alerts when you are asleep and/or not near a television, AM/FM radio, PC, or mobile device.  (Television and AM/FM radio require actively watching or listening to the broadcast.)

I recommend getting a SAME-capable desktop weather radio and then putting it on your nightstand or somewhere else you can hear it.  Configure the SAME-capable weather radio for your county, enable relevant alerts, disable irrelevant alerts (e.g., the flood ones if your home is too high for floodwater to reach), and change the battery annually.  (I prefer to change mine when Daylight Saving Time starts, to give it a fresh battery just before severe weather season.)

I also recommend getting an emergency radio with a solar panel and hand crank for your storm shelter or downstairs closet.

### Other Methods

You should also have at least 2 other ways to receive weather alerts in addition to a weather radio, in case one of them fails.

#### Wireless Emergency Alerts

Most modern smartphones are capable of receiving [Wireless Emergency Alerts](https://en.wikipedia.org/wiki/Wireless_Emergency_Alerts).  The mobile telephone network is less reliable than NOAA Weather Radio, but you should use WEA anyway as a fallback.  Check your smartphone's settings.

#### Local Broadcast Media

During severe weather, local, news-producing over-the-air television stations interrupt regular programming to cover it.  The coverage includes radar imagery and reports from the station's storm chasers.  A normal radio station that carries a news or news/talk format may also cover severe weather, which is useful during a power failure.

It is important that the station has a news department, though &mdash; music stations on the FM radio band and over-the-air television stations without news departments (usually those not affiliated with the 4 major networks) will often air only the bare-minimum <abbr title="Emergency Alert System">EAS</abbr> relay of an alert, which originates from &mdash; you guessed it &mdash; NOAA Weather Radio!  Some [low-power stations](https://en.wikipedia.org/wiki/Low-power_broadcasting) don't even <em>have</em> EAS equipment.

#### Weather Web Sites and Mobile Apps

Trusted weather Web sites and mobile apps can also give alerts.  Internet connectivity often fails during severe weather, though.  Personally, I simply use [the National Weather Service's Web site](https://www.weather.gov/).

#### Outdoor Warning Sirens

This is the absolute worst method.  Sirens are for outdoor use only.  They are not intended to be heard indoors, and the storm itself may make them difficult to hear.  It also provides no additional information.

### When You Receive a Warning

What to do when you receive a warning is outside of the scope of this blog post.  [weather.gov's Safety Tips section](https://www.weather.gov/safety/) should help.

### External Links

* [You Need to Buy a Weather Radio](https://web.archive.org/web/20230209031432/http://thevane.gawker.com/you-need-to-buy-a-weather-radio-1718320723)
* [The Siren Mentality](https://www.alabamawx.com/?p=35771)
* [Siren Mentality — It may be more dangerous than the storms](https://ksstorm.info/aware-2021-siren-mentality/)
