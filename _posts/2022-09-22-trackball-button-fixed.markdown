---
layout: post
title: "Trackball Button Fixed"
date: 2022-09-22 19:00 -0700
category: computing
tags: repair
---
I just fixed my Logitech TrackMan Marble's left button.

The button developed an issue where a single-click would register as a double-click, and click-and-drag was unreliable.  I opened the trackball, determined the model number of the button to be D2FC-F-7N, and obtained replacement buttons of that type.

I removed the old button and then soldered in the new one.  A quick check with [xev](https://manpage.me/?q=xev) confirmed that the new button worked.

The left button is now reliable again.
