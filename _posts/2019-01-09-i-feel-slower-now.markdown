---
layout: post
title: "I Feel Slower Now"
date: 2019-01-09 19:46 -0800
category: computing
tags: openbsd rant
---
During the first week of 2019, [nomad](http://wlair.us.to/my-computers/) ran OpenBSD 6.4.  Here is how that went:

**The good:** Unlike [under FreeBSD 10.3 a few years earlier](http://wlair.us.to/blog/2016/05/17/freebsd-vs-debian-on-nomad/index.html), hardware support was better.  The Wi-Fi signal strength thing turned out to be i3status not supporting `%quality` under the BSDs.  5 GHz worked fine.  Suspension and hibernation worked well.  OpenBSD also does not have the headphone jack switching issue that FreeBSD has.

**The bad:** Performance was unacceptably slow.  If audio and/or video was playing, loading a Web page, even a simple one, in Chromium would cause the audio to stutter.  Swapping would occur when memory usage reached roughly 1.5 GB (nomad has 3 GB) despite adding my user account to `staff` and adjusting `staff`'s resource limits in `/etc/login.conf`.  It would avoid the rest of the RAM as if it were poison or something.  Wi-Fi was about half the speed it was under GNU/Linux for some reason.  Also, some Web sites, such as Twitch streams (mpv + youtube-dl worked fine, though) and Internet Archive's JavaScript implementation of MAME, would not work in either Chromium (with sandboxing) or Firefox.

I have reverted to Debian GNU/Linux in the meantime.  OpenBSD mostly worked, and I would have stayed with it, if not for the above issue.
