---
layout: post
title: "wlair.us.to 14.1"
date: 2019-01-09 18:43 -0800
category: meta
---
Welcome to wlair.us.to 14.1!

The new design features a darker colour scheme and less visual clutter, and should work better with mobile devices.  Also, the main page now consists of a more typical blog-style paginated listing instead of a list of post titles.

Next, I should implement SSL support...  &gt;_&gt;
