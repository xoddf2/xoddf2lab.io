---
layout: post
title: "Spring Forward 2022"
date: 2022-03-13 00:56 -0800
category: journal
tags: dst
---
I just reset my clocks for Daylight Saving Time.  I also [changed](https://twitter.com/NWS/status/1502878298296180738) the batteries in my [weather radio](https://midlandusa.com/collections/weather/products/wr-120-weather-radio).

As I was setting the clocks, I noticed that the save battery in my copy of <cite>Pokémon Gold Version</cite> was empty.  I soldered a new CR2025 battery into it.  I had installed the old one in October 2011 using the tape method.  (I even had [an NES and Super NES](https://bulbapedia.bulbagarden.net/wiki/Game_system) in the old save file.  :-()  That battery lasted a few years longer than I expected, as did the original battery.
