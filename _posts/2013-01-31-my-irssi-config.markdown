---
layout: post
title: "My irssi Config"
date: 2013-01-31 02:26 -0500
category: computing
tags: irc irssi
---
Recently, I customised irssi. 

* I customised the theme a bit. I originally started with [more\_readable.theme][1], but eventually decided to make my own theme consisting of the default with some colours changed.
* I installed [hilightwin.pl][2]. I did not give it a split window. I instead put it in its own window, numbered 2.
* I finally got it to log the status window (`/window log on ~/irclogs/status.log`).
* Shockingly, I installed [nickcolor.pl][3]. I had disavowed nick colouring for over a year, after I switched back to irssi from WeeChat.

The config is at [GitHub][4].

[Screen shot of irssi][5]

[1]: http://www.irssi.org/themefiles/more_readable.theme
[2]: http://scripts.irssi.org/scripts/hilightwin.pl
[3]: http://scripts.irssi.org/scripts/nickcolor.pl
[4]: https://github.com/woddfellow2/configs/tree/master/.irssi
[5]: http://web.archive.org/web/20160512123117/http://wlair.us.to/remote/img/shots/irssi.png
