---
layout: post
title: "Arch Linux on genesis"
date: 2013-02-27 23:04 -0500
category: computing
tags: rofldell archlinux
---
A few days ago, I installed Arch on what was formerly my Gentoo box. I noticed that [a terrible bug in nouveau][1] was still present under Arch, therefore I shamefully switched to the proprietary NVIDIA driver. I am considering uninstalling X on my old box to use a framebuffer.

Also, I now have only 2 boxes again.

[1]: https://bugs.gentoo.org/show_bug.cgi?id=456682
