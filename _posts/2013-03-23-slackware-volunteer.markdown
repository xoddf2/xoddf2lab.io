---
layout: post
title: "Slackware on volunteer"
date: 2013-03-23 11:56 -0400
category: computing
tags: slackware oldbox
---
A while ago, I installed Slackware on my old box. At one point, I accidentally switched to -current because I chose the slackpkg mirror too carelessly.

I also got a tuner card. It worked under Arch, but [not under Slackware][1]. I still have yet to fix the problem.

[1]: http://www.linuxquestions.org/questions/slackware-14/failed-to-open-dev-video0-4175454619/
