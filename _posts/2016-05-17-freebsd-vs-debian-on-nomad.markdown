---
layout: post
title: "FreeBSD vs Debian on nomad"
date: 2016-05-17 01:24 -0000
category: computing
tags: freebsd debian nomad
---
The first few weeks after I got [nomad](http://wlair.us.to/my-computers/), it ran FreeBSD 10.3.  After that, I tried Debian GNU/Linux 8.4, and certain things worked better.

### FreeBSD

Most things I tried worked.  For some reason, Wi-Fi signal strength wasn't reported, and only 2.4 GHz networks were supported.  Some of the various Fn hotkeys (e.g., Fn+F7) didn't work.  Some issues weren't the fault of FreeBSD itself, but of unportable software, such as Emacs's display-battery-mode not working.  Also, there was a regression starting with FreeBSD 10.x in which QEMU no longer supported KVM, suggesting that users use bhyve or VirtualBox instead.

Suspension barely worked,  To get the TrackPoint to work after resuming, I had to switch to a text console and then back to X.  Also, sometimes suspension would fail, and would need a reboot to be able to suspend again.  I also once left VirtualBox running overnight when suspended, and I awoke to a kernel panic.  Hibernation also didn't work.

Another issue is that the audio couldn't switch between the internal speaker and the headphone jack.  This is not hardware-specific, as a desktop I used to have that ran FreeBSD also had this issue.

### Debian

I then installed Debian on another hard drive I had as a test, and I noticed that certain hardware worked better.  Wi-Fi signal strength is reported properly, and I can connect to 5 GHz networks.  The hotkeys that didn't work under FreeBSD do work under Debian.  Audio switching works properly.  Suspension works properly, and appears to be faster than under FreeBSD.  Hibernation seems to work.  QEMU can use KVM.

The only issues are that the Wi-Fi chip needs the non-free [firmware-iwlwifi](https://packages.debian.org/jessie/firmware-iwlwifi) package, and that [systemd occasionally hangs at halt and reboot](https://freedesktop.org/wiki/Software/systemd/Debugging/#shutdowncompleteseventually).  The latter appears to be a common issue and not hardware-specific.

### Both

The card reader works under both FreeBSD and Debian, at least with the SD card I borrowed from my digital camera.

I have yet to try the fingerprint reader or the ExpressCard slot.  It also has a dial-up modem, but it is likely to be a softmodem, and I have no dial-up connection with which to test it, this being the 2010s.

### External Links

* [X200 category at ThinkWiki](http://www.thinkwiki.org/wiki/Category:X200)
* [The scratchiest neckbeard, or FreeBSD on my Thinkpad X200](http://unethicalblogger.com/2013/12/03/scratchiest-neckbeard-freebsd-x200.html)
