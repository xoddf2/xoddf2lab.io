---
layout: post
title: "Arch Box Changes"
date: 2013-01-15 04:29 -0500
category: computing
tags: archlinux oldbox
---
Over a week ago, I noticed that my Arch Linux box's /home hard drive had started to fail. I then rsync'd its data to my Slackware box. I lost barely any data. I then removed that drive, and made the smaller / hard drive the master.

A few days later, I decided to reinstall Arch Linux, because it would feel cleaner. I burned CDs of DBAN 2.2.7 and the 4 January 2013 edition of the Arch install/rescue media, DBANned the hard drive, and reinstalled Arch. The CD drive kept throwing I/O errors, therefore I had to boot with the CD and then have a USB flash drive with the same install medium continue from there.

The nouveau driver works this time, therefore I now get the 1280\*960 screen resolution in X again.
