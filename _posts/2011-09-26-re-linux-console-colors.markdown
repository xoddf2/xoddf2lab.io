---
layout: post
title: "Re: Linux Console Colors"
date: 2011-09-26 04:28 -0500
published: false
---
Over [here](http://phraktured.net/linux-console-colors.html) is a page explaining how to change the colours of the Linux framebuffer console (I am not sure about the VGA console).

My `~/.bashrc` contains the following:

{% highlight shell %}
# Zenburn colour scheme
function zenburn {
	if [ "$TERM" = "linux" ]; then
		echo -en "\e]P01E2320" #black
		echo -en "\e]P8709080" #darkgrey
		echo -en "\e]P1705050" #darkred
		echo -en "\e]P9dca3a3" #red
		echo -en "\e]P260b48a" #darkgreen
		echo -en "\e]PAc3bf9f" #green
		echo -en "\e]P3dfaf8f" #brown
		echo -en "\e]PBf0dfaf" #yellow
		echo -en "\e]P4506070" #darkblue
		echo -en "\e]PC94bff3" #blue
		echo -en "\e]P5dc8cc3" #darkmagenta
		echo -en "\e]PDec93d3" #magenta
		echo -en "\e]P68cd0d3" #darkcyan
		echo -en "\e]PE93e0e3" #cyan
		echo -en "\e]P7dcdccc" #lightgrey
		echo -en "\e]PFffffff" #white
		clear #for background artifacting
	fi
}

# Normal colour scheme
function normalcolours {
	if [ "$TERM" = "linux" ]; then
		echo -en "\e]P0000000" #black
		echo -en "\e]P8555555" #darkgrey
		echo -en "\e]P1AA0000" #darkred
		echo -en "\e]P9FF5555" #red
		echo -en "\e]P200AA00" #darkgreen
		echo -en "\e]PA55FF55" #green
		echo -en "\e]P3AA5500" #brown
		echo -en "\e]PBFFFF55" #yellow
		echo -en "\e]P40000AA" #darkblue
		echo -en "\e]PC5555FF" #blue
		echo -en "\e]P5AA00AA" #darkmagenta
		echo -en "\e]PDFF55FF" #magenta
		echo -en "\e]P600AAAA" #darkcyan
		echo -en "\e]PE55FFFF" #cyan
		echo -en "\e]P7AAAAAA" #lightgrey
		echo -en "\e]PFFFFFFF" #white
		clear #for background artifacting
	fi
}
{% endhighlight %}

I put the colour-changing commands in each colour scheme's own function. This way, for example, to switch to the Zenburn colour scheme above, I would type:

    $ zenburn

...at the prompt. This has a few advantages over just putting it bare in `~/.bashrc`:
* This allows multiple colour schemes. Above you see Zenburn and the standard colour scheme.
* The normalcolours function seen here, which contains the standard Linux console/CGA colours, allows one to revert the virtual console back to its normal colour scheme.

My `~/.bash_logout` contains the following:

{% highlight shell %}
if [ "$TERM" = "linux" ]; then
	normalcolours
fi
{% endhighlight %}

This reverts the colours back to normal at logout if I changed them. One could put something similar in `~/.bash_login` to change the colour scheme automatically at login.
