---
layout: post
title: "Now with HTTPS Support (finally!)"
date: 2020-12-08 12:53 -0800
category: meta
---
wlair.us.to now supports HTTPS.  Insecure plain HTTP is still supported for compatibility with older software.

Implementing it was [easier than I expected](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html#enabling-lets-encrypt-integration-for-your-custom-domain).  I ran into [a "Something went wrong" error](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html#troubleshooting) at first, but retrying worked.

I had wanted to support HTTPS [for a long while](https://wlair.us.to/blog/2019/01/09/wlair141/index.html), and it has finally happened.
