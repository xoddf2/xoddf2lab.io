---
layout: post
title: "Moved to GitLab"
date: 2018-06-09 18:02 -0700
category: meta
---
I just finished migrating [my repositories](https://gitlab.com/users/xoddf2/projects) (including this Web site) from GitHub to GitLab.  For now, nothing else has changed.

No, the Microsoft acquisition was not the reason.
