---
layout: post
title: "Server Move"
date: 2013-04-25 21:52 -0000
category: meta
---
I have moved wlair.us.to from [Meskarune's personal VPS][1] (maharani.meskarune.com) to the [Hacker Haven][2] server (tesseract.hackerhaven.net). Beware that in some cases, it may take 24–48 hours for the new DNS record to propagate.

[1]: http://meskarune.com/
[2]: http://hackerhaven.net/
