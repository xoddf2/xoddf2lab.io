---
layout: post
title: "mu4e Polishing"
date: 2018-03-08 15:45 -0800
category: computing
tags: email mu4e emacs
---
Until recently, [mu4e](http://www.djcbsoftware.nl/code/mu/mu4e.html)'s "Unread messages" bookmark gave me messages from Gmail's <span style="font-family: monospace;">/[Gmail].Spam</span> folder, and duplicates of legitimate emails from both the <span style="font-family: monospace;">/INBOX</span> and <span style="font-family: monospace;">/Gmail.[All Mail]</span> folders.  Today, I fixed that.

After doing some Web searches, I found [someone's configuration](https://vxlabs.com/2017/02/07/mu4e-0-9-18-e-mailing-with-emacs-now-even-better/), and copied and then edited the <span style="font-family: monospace;">mu4e-bookmarks</span> variable from it.  I added some "<span style="font-family: monospace;">AND NOT [maildir goes here]</span>" directives, and it worked.  I also added bookmarks to <span style="font-family: monospace;">/[Gmail].All Mail</span> and <span style="font-family: monospace;">/[Gmail].Sent Mail</span>.

I [added that to my own config](https://github.com/woddfellow2/configs/commit/6ad9c37d8c33aa0609077a79489eea1831503e04) and [now it works fine](https://imgur.com/833c2k0).

The only major thing left to fix: I must open an encrypted message twice to see it...
