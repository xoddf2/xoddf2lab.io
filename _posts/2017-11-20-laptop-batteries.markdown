---
layout: post
title: "Laptop Batteries"
date: 2017-11-20 10:15 -0800
category: computing
tags: nomad
---
When I got [nomad](http://wlair.us.to/my-computers/#nomad) in April 2016, it included a 6-cell battery.  [acpi](https://sourceforge.net/projects/acpiclient/) told me its "last full capacity" was 54%.  Eventually that ended up at 34%.

In early 2017, I replaced it with a 9-cell battery.

Recently, nomad's battery started behaving oddly.

Yesterday, I noticed that charging would stall at about 80%.  Upon recharging it later, acpi told me its "last full capacity" was 69% (down from 87%).  This morning, battery life suddenly jumped from the low 20s to 5%.

I may try [reconditioning](http://www.thinkwiki.org/wiki/Maintenance#Battery_treatment) it to see what happens.
