---
layout: post
title: "x11-ssh-askpass HOWTO"
date: 2013-04-15 16:22 -0000
category: computing
tags: x11-ssh-askpass
---
I discovered this in OpenBSD. There, by default, at the beginning of an X session, it asks for the SSH key passphrase if there is a key. It does this with [x11-ssh-askpass][1]. I [installed it][3] on both of my boxes ([the SlackBuild of x11-ssh-askpass intended for Slackware 12.1][2] still works as of Slackware 14.0), and borrowed this from OpenBSD's default ~/.xinitrc:

{% highlight shell %}
# ssh-agent (Inspired by and stolen from OpenBSD)
id1=$HOME/.ssh/identity
id2=$HOME/.ssh/id_dsa
id3=$HOME/.ssh/id_rsa
id4=$HOME/.ssh/id_ecdsa
if [ -x /usr/bin/ssh-agent ] && [ -f $id1 -o -f $id2 -o -f $id3 -o -f $id4 ];
then
	eval $(ssh-agent -s)
	ssh-add < /dev/null
fi

# Insert window-manager line here...

# Kill ssh-agent upon exiting
if [ "$SSH_AGENT_PID" ]; then
	ssh-add -D < /dev/null
	eval $(ssh-agent -s -k)
fi
{% endhighlight %}

...and then it worked.

[1]: http://www.jmknoble.net/software/x11-ssh-askpass/
[2]: http://slackbuilds.org/repository/12.1/network/x11-ssh-askpass/
[3]: https://www.archlinux.org/packages/extra/i686/x11-ssh-askpass/
